package com.example.intersoft_android2.chater.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by intersoft-android2 on 11-11-2016.
 */

public class Data {
    @SerializedName("error")
    private String error;
    @SerializedName("uid")
    private String uid;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


class user{
    @SerializedName("name")
    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @SerializedName("email")
    private String email;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
}

}
