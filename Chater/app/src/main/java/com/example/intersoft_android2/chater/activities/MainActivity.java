package com.example.intersoft_android2.chater.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.intersoft_android2.chater.R;
import com.example.intersoft_android2.chater.activities.Signup;

import static android.support.v4.accessibilityservice.AccessibilityServiceInfoCompat.getId;

public class MainActivity extends Activity implements View.OnClickListener {


    public Button signup,login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }

    private void init() {
        signup=(Button)findViewById(R.id.signup);
        login=(Button)findViewById(R.id.login);
        signup.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.signup :

              Intent intent=new Intent(this,Signup.class);
              startActivity(intent);
              break;
          case R.id.login:
              Intent logintent=new Intent(this,Login.class);
              startActivity(logintent);
              break;
          default:break;
      }
    }
}
