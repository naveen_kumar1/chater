package com.example.intersoft_android2.chater.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.intersoft_android2.chater.R;

/**
 * Created by intersoft-android2 on 10-11-2016.
 */

public class Login extends AppCompatActivity implements View.OnClickListener {


    public Button signin;
    public TextView createnew;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        init();
    }

    private void init() {
        signin=(Button)findViewById(R.id.signin);
        createnew=(TextView)findViewById(R.id.newaccount);
        signin.setOnClickListener(this);
        createnew.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signin:
                break;
            case  R.id.newaccount:
                Intent intent=new Intent(this,Signup.class);
                finish();
                startActivity(intent);
                break;
            default:
                break;


        }
    }
}
