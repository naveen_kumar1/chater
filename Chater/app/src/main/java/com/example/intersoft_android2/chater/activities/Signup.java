package com.example.intersoft_android2.chater.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.intersoft_android2.chater.R;
import com.example.intersoft_android2.chater.model.Data;
import com.example.intersoft_android2.chater.retrofit.ApiClient;
import com.example.intersoft_android2.chater.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by intersoft-android2 on 10-11-2016.
 */

public class Signup extends AppCompatActivity implements View.OnClickListener {

    public Button signup;
    public TextView existuser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        init();
    }

    private void init() {
        signup=(Button)findViewById(R.id.signup);
        existuser=(TextView)findViewById(R.id.existuser);
        signup.setOnClickListener(this);
        existuser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signup:

                createUser();
                break;
            case  R.id.existuser:

                Intent intent=new Intent(this,Login.class);
                finish();
                startActivity(intent);
                break;
            default:
                break;


        }
    }

    private void createUser() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Data> call = apiService.createUser("shubham","m@yopmail.com","123456");
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data>call, Response<Data> response) {

                Log.d("heelo", "Number of movies received: " + response.body());
 //               Log.d("heelo", "Number of movies received: " + "body");
            }

            @Override
            public void onFailure(Call<Data>call, Throwable t) {
                // Log error here since request failed
                Log.e("Hello", "aaaaaaa"+t.getMessage()+"");//t.toString());
            }
        });
    }
}
