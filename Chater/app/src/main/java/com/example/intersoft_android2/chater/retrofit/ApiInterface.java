package com.example.intersoft_android2.chater.retrofit;

/**
 * Created by intersoft-android2 on 11-11-2016.
 */


import com.example.intersoft_android2.chater.model.Data;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface ApiInterface {
    @FormUrlEncoded
    @POST("register.php")
    Call<Data> createUser(@Field("name") String name, @Field("email") String email, @Field("password") String password);
//
//    @GET("movie/{id}")
//    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
